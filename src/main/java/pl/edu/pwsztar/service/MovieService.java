package pl.edu.pwsztar.service;

import pl.edu.pwsztar.domain.dto.CreateFileDto;
import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.dto.MovieCounterDto;
import pl.edu.pwsztar.domain.dto.MovieDto;

import java.io.IOException;
import java.util.List;

public interface MovieService {

    List<MovieDto> findAll();

    void createMovie(CreateMovieDto createMovieDto);

    void deleteMovie(Long movieId);

    MovieCounterDto countMovies();

    CreateFileDto createTxtFileWithMovies() throws IOException;
}
