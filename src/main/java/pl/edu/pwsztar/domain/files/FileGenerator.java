package pl.edu.pwsztar.domain.files;

import pl.edu.pwsztar.domain.dto.CreateFileDto;
import pl.edu.pwsztar.domain.dto.FileDto;

import java.io.IOException;

public interface FileGenerator {
    CreateFileDto toTxt(FileDto fileDto) throws IOException;
}
