package pl.edu.pwsztar.domain.files;

import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.CreateFileDto;
import pl.edu.pwsztar.domain.dto.FileDto;
import pl.edu.pwsztar.domain.dto.MovieDto;
import pl.edu.pwsztar.domain.files.FileGenerator;

import java.io.*;
import java.util.Date;

@Service
public class TextFileGenerator implements FileGenerator {

    private static final String FILE_NAME_PREFIX = "movies_";
    private static final String FILE_EXTENSION = ".txt";
    private static final String FILE_CONTENT_SEPARATOR = " ";

    @Override
    public CreateFileDto toTxt(FileDto fileDto) throws IOException {
        final File file = writeToFile(fileDto);

        InputStream inputStream = new FileInputStream(file);
        InputStreamResource inputStreamResource = new InputStreamResource(inputStream);

        return new CreateFileDto.Builder()
                .resource(inputStreamResource)
                .contentLength(file.length())
                .fileName(getFileName())
                .fileExtension(FILE_EXTENSION)
                .build();
    }

    private File writeToFile(FileDto fileDto) throws IOException {
        File file = File.createTempFile(FILE_NAME_PREFIX, FILE_EXTENSION);
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(fileOutputStream));

        for (MovieDto movie : fileDto.getMovies()) {
            bufferedWriter.write(movie.getYear() + FILE_CONTENT_SEPARATOR + movie.getTitle());
            bufferedWriter.newLine();
        }

        bufferedWriter.close();
        fileOutputStream.flush();
        fileOutputStream.close();

        return file;
    }

    private String getFileName() {
        return FILE_NAME_PREFIX + (new Date().getTime());
    }
}

