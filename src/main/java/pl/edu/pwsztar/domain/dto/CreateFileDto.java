package pl.edu.pwsztar.domain.dto;

import org.springframework.core.io.InputStreamResource;

public class CreateFileDto {
    private InputStreamResource resource;
    private Long contentLength;
    private String fileName;
    private String fileExtension;

    private CreateFileDto(Builder builder) {
        resource = builder.resource;
        contentLength = builder.contentLength;
        fileName = builder.fileName;
        fileExtension = builder.fileExtension;
    }

    public static Builder builder() {
        return new Builder();
    }

    public InputStreamResource getResource() {
        return resource;
    }

    public Long getContentLength() {
        return contentLength;
    }

    public String getFileName() {
        return fileName;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public static final class Builder {
        private InputStreamResource resource;
        private Long contentLength;
        private String fileName;
        private String fileExtension;

        public Builder() {
        }

        public Builder resource(InputStreamResource resource) {
            this.resource = resource;
            return this;
        }

        public Builder contentLength(Long contentLength) {
            this.contentLength = contentLength;
            return this;
        }

        public Builder fileName(String fileName) {
            this.fileName = fileName;
            return this;
        }

        public Builder fileExtension(String fileExtension) {
            this.fileExtension = fileExtension;
            return this;
        }

        public CreateFileDto build() {
            return new CreateFileDto(this);
        }
    }
}
